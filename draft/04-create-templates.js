const GLClient = require("./gl-cli").GLClient;
const tools = require("./gl-cli").tools;
const config = require("./config").config;

let glClient = new GLClient({
  baseUri: `${config.url}/api/v4`,
  token: config.token
});

function createFile({
  projectId,
  projectName,
  nameSpace,
  filePath,
  branch,
  content,
  commitMessage
}) {
  let id = projectId
    ? projectId
    : `${nameSpace}%2F${projectName
        .replaceAll(".", "%2E")
        .replaceAll("-", "%2D")
        .replaceAll("_", "%5F")}`;
  let path = filePath
    .replaceAll("/", "%2F")
    .replaceAll(".", "%2E")
    .replaceAll("-", "%2D")
    .replaceAll("_", "%5F");
  let commit_message = commitMessage
    .replaceAll("/", "%2F")
    .replaceAll(".", "%2E")
    .replaceAll("-", "%2D")
    .replaceAll("_", "%5F");
  let branch_name = branch
    .replaceAll("/", "%2F")
    .replaceAll(".", "%2E")
    .replaceAll("-", "%2D")
    .replaceAll("_", "%5F");

  return glClient
    .post({
      path: `/projects/${id}/repository/files/${path}?branch=${branch_name}&commit_message=${commit_message}`,
      data: {
        content: content
      }
    })
    .then(response => {
      return response;
    })
    .catch(error => {
      return error;
    });
}

// see https://github.com/stevemao/github-issue-templates

async function generateTemplates() {

  await createFile({
    nameSpace: config.group,
    projectName: config.project,
    filePath: ".gitlab/issue_templates/Feature-Request.md",
    branch: "master",
    content: tools.formatText(`
      ---
      name: 🚀 Feature Request
      about: I have a suggestion (and may want to implement it 🙂)!

      ---

      ## Feature Request

      **Is your feature request related to a problem? Please describe.**
      
      > A clear and concise description of what the problem is. Ex. I have an issue when [...]

      **Describe the solution you'd like**
      
      > A clear and concise description of what you want to happen. Add any considered drawbacks.

      **Describe alternatives you've considered**
      
      > A clear and concise description of any alternative solutions or features you've considered.

      **Teachability, Documentation, Adoption, Migration Strategy**
      
      > If you can, explain how users will be able to use this and possibly write out a version the docs.
      > Maybe a screenshot or design?

      /label ~type_feature
      /cc @k33g
      /assign @babs @buster

    `),
    commitMessage: ":tada: add issue template"
  });

  await createFile({
    nameSpace: config.group,
    projectName: config.project,
    filePath: ".gitlab/issue_templates/Bug-NodeJS.md",
    branch: "master",
    content: tools.formatText(`
      ---
      name: 🐛 Bug Report - JavaScript version
      about: If something isn't working as expected 🤔.
      ---
      ## Bug Report

      **Current Behavior**
      
      > A clear and concise description of the behavior.

      **Input Code**

      - REPL or Repo link if applicable:

      \`\`\`js
      var your => (code) => here;
      \`\`\`

      **Expected behavior/code**

      > A clear and concise description of what you expected to happen (or code).

      **Configuration (.babelrc, package.json, cli command)**

      \`\`\`js
      {
          "your": { "config": "here" }
      }
      \`\`\`

      **Environment**

      > - Babel version(s): [e.g. v6.0.0, v7.0.0-beta.34]
      > - Node/npm version: [e.g. Node 8/npm 5]
      > - OS: [e.g. OSX 10.13.4, Windows 10]
      > - Monorepo [e.g. yes/no/Lerna]

      **Possible Solution**

      > Only if you have suggestions on a fix for the bug

      **Additional context/Screenshots**

      > Add any other context about the problem here. If applicable, add screenshots to help explain.

      /label ~type_bug
      /cc @k33g
      /assign @babs @buster
    `),
    commitMessage: ":tada: add bug template template"
  });
}

generateTemplates()
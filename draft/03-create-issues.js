const GLClient = require("./gl-cli").GLClient;
const tools = require("./gl-cli").tools;
const config = require("./config").config;

let glClient = new GLClient({
  baseUri: `${config.url}/api/v4`,
  token: config.token
});

function createIssue({
  title,
  description,
  confidential,
  assignee_ids,
  milestone_id,
  labels,
  created_at,
  due_date,
  weight,
  userOrGroup,
  project
}) {
  return glClient
    .post({
      path: `/projects/${tools.projectUrlEncodedPath({
        userOrGroup,
        project
      })}/issues`,
      data: {
        title,
        description,
        confidential,
        assignee_ids,
        milestone_id,
        labels,
        created_at,
        due_date,
        weight,
        userOrGroup,
        project
      }
    })
    .then(response => {
      return response.data;
    })
    .catch(err => {
      return error;
    });
}

function getMilestones({ userOrGroup, project }) {
  return glClient
    .get({
      path: `/projects/${tools.projectUrlEncodedPath({
        userOrGroup,
        project
      })}/milestones`
    })
    .then(response => {
      return response.data;
    })
    .catch(err => {
      return error;
    });
}

function fetchUser({ handle }) {
  return glClient
    .get({
      path: `/users?username=${handle}`
    })
    .then(response => {
      return response.data[0];
    })
    .catch(err => {
      return error;
    });
}

let group = config.group;
let project = config.project;
let handle = config.handle;

async function task(issue, user, group, project) {
  issue.assignee_ids = [user.id];
  issue.userOrGroup = group;
  issue.project = project;
  issue.created_at = new Date().toDateString();
  issue.confidential = false;

  await createIssue(issue).then(res => console.log(res));
}

async function generateIssues(handle, group, project) {
  let user = await fetchUser({ handle: handle });
  let milestones = await getMilestones({
    userOrGroup: group,
    project: project
  });

  await task(
    {
      title: "plan",
      description: "plan",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 1")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 06, 23).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "design",
      description: "design",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 1")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 06, 24).toDateString(),
      weight: 2
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "test",
      description: "test",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 1")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 06, 27).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "deploy",
      description: "deploy",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 1")
        .id,
      labels: "type_infrastructure,progress_0%",
      due_date: new Date(2018, 06, 27).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "review",
      description: "review",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 1")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 06, 27).toDateString(),
      weight: 3
    },
    user,
    group,
    project
  );

  // Sprint 2

  await task(
    {
      title: "plan",
      description: "plan",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 2")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 06, 30).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "design",
      description: "design",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 2")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 06, 31).toDateString(),
      weight: 2
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "test",
      description: "test",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 2")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 07, 3).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "deploy",
      description: "deploy",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 2")
        .id,
      labels: "type_infrastructure,progress_0%",
      due_date: new Date(2018, 07, 3).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "review",
      description: "review",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 2")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 07, 3).toDateString(),
      weight: 3
    },
    user,
    group,
    project
  );

  // Sprint 3

  await task(
    {
      title: "plan",
      description: "plan",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 3")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 07, 6).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "design",
      description: "design",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 3")
        .id,
      labels: "type_documentation,progress_0%",
      due_date: new Date(2018, 07, 7).toDateString(),
      weight: 2
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "test",
      description: "test",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 3")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 07, 10).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "deploy",
      description: "deploy",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 3")
        .id,
      labels: "type_infrastructure,progress_0%",
      due_date: new Date(2018, 07, 10).toDateString(),
      weight: 1
    },
    user,
    group,
    project
  );
  await task(
    {
      title: "review",
      description: "review",
      milestone_id: milestones.find(milestone => milestone.title == "Sprint 3")
        .id,
      labels: "type_test,progress_0%",
      due_date: new Date(2018, 07, 10).toDateString(),
      weight: 3
    },
    user,
    group,
    project
  );
}

generateIssues(handle, group, project);

// project labels
const GLClient = require("./libs/gl-cli").GLClient
require('dotenv').config({ path: './.env' })

let gitLab = new GLClient({
  baseUri: `${process.env.GITLAB_URL}/api/v4`,
  token: process.env.TOKEN
})

async function generateLabel(userOrGroup, project, label) {
  console.log(userOrGroup, project)
  label.userOrGroup = userOrGroup
  label.project = project

  await gitLab.createLabel(label)
    .then(response => {
      console.log("😀", response)
    })
    .catch(error => {
      console.log("😡", error.message)
    });
}

let labels = [
  {
    name: "Module 1: Introduction",
    color: "#5CB85C",
    description: "Introduction"
  },
  {
    name: "Module 2: Setup",
    color: "#5843AD",
    description: "Setup"
  },
  {
    name: "Module 3: First steps",
    color: "#428BCA",
    description: "First steps"
  },
  {
    name: "Module 4: To infinity and beyond!",
    color: "#D10069",
    description: "One more step"
  },
  {
    name: "Assessment",
    color: "#004E00",
    description: "Assessment"
  },
]

// https://gitlab.com/bots-garden/training-materials/litelement-first-steps

labels.forEach(
  label => 
    generateLabel(
      "bots-garden/training-materials", 
      "litelement-first-steps", 
      label
    )
)

// https://github.com/enquirer/enquirer
// project labels
const GLClient = require("./libs/gl-cli").GLClient
require('dotenv').config({ path: './.env' })

let gitLab = new GLClient({
  baseUri: `${process.env.GITLAB_URL}/api/v4`,
  token: process.env.TOKEN
})


async function generateMileStone(userOrGroup, project, sprint) {
  sprint.userOrGroup = userOrGroup;
  sprint.project = project;

  console.log(sprint)

  await gitLab.createMilestone(sprint)
    .then(response => {
      console.log("😀", response);
    })
    .catch(error => {
      console.log("😡", error);
    });
}

// https://gitlab.com/bots-garden/training-materials/litelement-first-steps

generateMileStone(
  "bots-garden/training-materials", 
  "litelement-first-steps", 
  {
    title: "Course progress",
    description: "Course progress",
    start_date: null,
    due_date: null
  }
)

/*
  {
    title: "Course progress",
    description: "Course progress",
    start_date: new Date(2018, 06, 23).toDateString(), // 05 = juin with JavaScript
    due_date: new Date(2018, 06, 27).toDateString() // 06 = juillet with JavaScript
  }
*/

